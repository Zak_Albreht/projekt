# Navodila za projektno nalogo pri OVS

Narediti morate statistično analizo podatkov in interpretirati dobljene rezultate. Rezultat vaše naloge naj bo poročilo v fomratu PDF, ki naj vsebuje tako kodo, spremno besedilo in rezultate ter grafe. Prosim, da uporabite enega od vzorcev: 
 * [vzorec za R](ImePriimek_VPISNA.rmd) v [Rmarkdown](https://rmarkdown.rstudio.com/), če delate v R-ju 
 * [vzorec za Python](ImePriimek_VPISNA.ipynb) za [Jupyter](https://jupyter.org/), če boste nalogo naredili v Pythonu.

Kodo naj spremlja besedilo, ki razloži rezultate, opiše kako do rezultatov pridemo in pove,  kaj sploh računamo.  Spremno besedilo naj ne bo podrobni opis kode, ampak naj bo razumljivo tudi tistim, ki ne znajo programirati.  Napisano naj bo tako, da bo tudi brez kode razvidno, kaj ste pri nalogi delali.  Pri oblikovanju besedila smiselno uporabite [Markdown](https://jupyter-notebook.readthedocs.io/en/stable/examples/Notebook/Working%20With%20Markdown%20Cells.html).


Oglejte si še [navodila za pripravo PDF](https://youtu.be/jCjP77iCOqQ) iz Jupiter zvezka.

## Izbira podatkov in metode

Katero metodo (t-test ali regresija) in katere podatke (.csv) morate uporabiti, določite s pomočjo zadnjih dveh števk vaše vpisne številke z uporabo naslednje tabele:

### t-test za neodvisna vzorca

| Fakulteta   |   Vpisna št. |   Podatki                    |   Ime                               |
|-------------|--------------|------------------------------|-------------------------------------|
| FRI         | 00 - 10      | [svinec.csv  ](podatki/svinec.csv  ) | [Količina svinca                    ](podatki/svinec.md  ) |
| FRI         | 22 - 31      | [geom.csv    ](podatki/geom.csv    ) | [Rezultati testa geometrije         ](podatki/geom.md    ) |
| FRI         | 43 - 51      | [premor.csv  ](podatki/premor.csv  ) | [Število napak                      ](podatki/premor.md  ) |
| FRI         | 62 - 71      | [industr.csv ](podatki/industr.csv ) | [Število proizvedenih polprevodnikov](podatki/industr.md ) |
| UI          | 19 - 99      | [urpostav.csv](podatki/urpostav.csv) | [Urna postavka natakarjev           ](podatki/urpostav.md) |
| FRI         | 80 - 85      | [avgume.csv  ](podatki/avgume.csv  ) | [Življenjska doba avtomobilskih gum ](podatki/avgume.md  ) |

### Linearna regresija

|Fakulteta  |   Vpisna št. |     Podatki                       | Ime                        |
|-----------|--------------|-----------------------------------|----------------------------|
|FRI        |   11 - 21    | [jezero.csv ](podatki/jezero.csv )| [Dolžina ribe              ](podatki/jezero.md ) |
|UI         |   01 - 18    | [avcena.csv ](podatki/avcena.csv )| [Prodajna cena avtomobila  ](podatki/avcena.md ) |
|FRI        |   32 - 42    | [mozgani.csv](podatki/mozgani.csv)| [Teža možganov pri sesalcih](podatki/mozgani.md) |
|FRI        |   52 - 61    | [klek.csv   ](podatki/klek.csv   )| [Višina kleka              ](podatki/klek.md   ) |
|FRI        |   72 - 79    | [zavor.csv  ](podatki/zavor.csv  )| [Zavorna pot               ](podatki/zavor.md  ) |
|FRI        |   86 - 99    | [forbes.csv ](podatki/forbes.csv )| [Zračni tlak               ](podatki/forbes.md ) |


## Dodatna gradiva

Morda vam pride prav tudi kateri od naslednjih dokumentov:

1. [Priročnik za delo z različnimi objekti v programu R](IntroR.pdf),
2. Primer statistične obdelave podatkov: [t-test za neodvisna vzorca](t-test.pdf),
3. Primer statistične obdelave podatkov: [linearna regresija](regresija.pdf),
4. [Opis podatkov](podatki/podatki.md),
5. [Jupyter zvezki z vaj](https://gitlab.com/ul-fri/ovs/python).

Če imate težave s programom R, kontaktirajte Kristino, v primeru težav s Pythonom pa Martina ali Aleksandro.


## Podrobnejša navodila: t-test za neodvisna vzorca

Seminarska naloga, v kateri se uporablja t-test za neodvisna vzorca, naj ima naslednjo strukturo:

1. Opis podatkov in raziskovalne domneve
2. Grafični prikaz podatkov - histogram in škatla z brki
3. Opisna statistika
4. Intepretacija grafov in opisne statistike
5. Definiranje ničelne in alternativne domneve
6. Preverjanje predpostavk t-testa za neodvisna vzorca
7. Intervala zaupanja za neznani povprečji dveh populacij
8. Rezultati t-testa za neodvisna vzorca in njihova interpretacija


## Podrobnejša navodila: Linearna regresija

Seminarska naloga, v kateri se uporablja linearna regresija, naj ima naslednjo strukturo:

1. Opis podatkov
2. Opisna statistika
3. Razsevni diagram in vzorčni koeficient korelacije
4. Formiranje linearnega regresijskega modela in preverjanje njegovih predpostavk
5. Testiranje linearnosti regresijskega modela in koeficient determinacije
6. Intervala zaupanja za naklon in odsek regresijske premice
7. Interval predikcije za vrednost Y pri izbrani vrednosti X

Če je bilo potrebno transformirati eno ali obe spremenljivki, prikažite opisno statistiko in rezultate linearnega regresijskega modela (razsevni diagram, diagnostične grafe) na začetku (brez transformacije) in po transformaciji, potem pa prediskutirajte, zakaj je transformacija podatkov bila potrebna. Če je transformirana slučajna spremenljivka Y, interval predikcije transformirajte nazaj (iz intervala predikcije za transformirano Y v interval predikcije za Y).


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licenca Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type">Projektna naloga pri predmetu OVS</span> avtorjev 
<a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/aleksandra-franc" property="cc:attributionName" rel="cc:attributionURL">Aleksandra Franc</a>,
<a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/kristina-veljkovic" property="cc:attributionName" rel="cc:attributionURL">Kristina Veljkovič</a> in
<a xmlns:cc="http://creativecommons.org/ns#" href="https://www.fri.uni-lj.si/sl/o-fakulteti/osebje/martin-vuk" property="cc:attributionName" rel="cc:attributionURL">Martin Vuk</a>
 je objavljeno pod <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">licenco Creative Commons Priznanje avtorstva-Nekomercialno-Deljenje pod enakimi pogoji 4.0 Mednarodna</a>.<br />Ustvarjeno na podlagi dela, dostopnega na <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/ul-fri/ovs/projekt/" rel="dct:source">https://gitlab.com/ul-fri/ovs/projekt/</a>.<br />Dodatna dovoljenja, ki presegajo obsege te licence, so dostopna na <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md" rel="cc:morePermissions">https://gitlab.com/ul-fri/ovs/projekt/-/blob/master/LICENSE.md</a>.