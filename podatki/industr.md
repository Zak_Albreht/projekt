# Število proizvedenih polprevodnikov

Podatki: [industr.csv](industr.csv)

## Opis

Meritve števila proizvedenih polprevodnikov na naslednjih vzorcih delavcev

1. vzorec 20 delavcev, ki so izdelovali polprevodnike po stari metodi in
2. vzorec 30 delavcev, ki so izdelovali polprevodnike po novi metodi.

## Format

Baza podatkov s 50 meritvami dveh spremenljivk

* *metoda* je nominalna spremenljivka, z numeričnima kodama: 1=stara metoda, 2=nova
metoda.
* *polprev* je numerična diskretna spremenljivka, ki predstavlja število proizvedenih polpre-
vodnikov.

## Raziskovalna domneva

Nova metoda vpliva na zvišanje števila proizvedenih polprevodnikov.